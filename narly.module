<?php

/**
 * @file
 * Allows content-permissions to be rebuilt without wiping node-access table.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Add a checkbox to the node-access-rebuild confirmation form, exposing option
 * to wipe node_access table.
 */
function narly_form_node_configure_rebuild_confirm_alter(&$form, &$form_state, $form_id) {
  if (!isset($form['confirm']['#weight'])) {
    $form['confirm']['#weight'] = 0;
  }
  $form['wipe_node_access_table'] = array(
    '#title' => t('Wipe node_access table first?'),
    '#description' => t('Check this box if you want to wipe the node_access
      table before rebuilding content permissions. You might want to leave
      this unchecked if you have a site with a large number of nodes, as pages
      might appear broken while the table is being repopulated. If you choose
      to check this box, consider putting the site in
      <a href="/admin/config/development/maintenance">maintenance mode</a>
      before proceeding.'),
    '#type' => 'checkbox',
    '#weight' => ($form['confirm']['#weight'] + 1),
  );
  // We replace the node module's form-submit callback with our own.
  $form['#submit'] = array('narly_node_configure_rebuild_confirm_submit');
}

/**
 * Replacement submit handler for the node-access-rebuild confirmation form.
 */
function narly_node_configure_rebuild_confirm_submit($form, &$form_state) {
  if ($form_state['values']['wipe_node_access_table'] === 1) {
    node_access_rebuild(TRUE);
  }
  else {
    narly_node_access_rebuild(TRUE);
  }
  $form_state['redirect'] = 'admin/reports/status';
}

/**
 * Rebuild content permissions without wiping the node_access table first.
 *
 * This is a straight copy of the node_access_rebuild() function in
 * node.module, but with the first line removed.
 */
function narly_node_access_rebuild($batch_mode = FALSE) {
  // Only recalculate if the site is using a node_access module.
  if (count(module_implements('node_grants'))) {
    if ($batch_mode) {
      $batch = array(
        'title' => t('Rebuilding content access permissions'),
        'operations' => array(
          array('_node_access_rebuild_batch_operation', array()),
        ),
        'finished' => '_node_access_rebuild_batch_finished',
      );
      batch_set($batch);
    }
    else {
      // Try to allocate enough time to rebuild node grants.
      drupal_set_time_limit(240);

      // Rebuild newest nodes first so that recent content becomes available
      // quickly.
      $nids = db_query("SELECT nid FROM {node} ORDER BY nid DESC")->fetchCol();
      foreach ($nids as $nid) {
        $node = node_load($nid, NULL, TRUE);
        // To preserve database integrity, only acquire grants if the node
        // loads successfully.
        if (!empty($node)) {
          node_access_acquire_grants($node);
        }
      }
    }
  }
  else {
    // Not using any node_access modules. Add the default grant.
    db_insert('node_access')
      ->fields(array(
        'nid' => 0,
        'realm' => 'all',
        'gid' => 0,
        'grant_view' => 1,
        'grant_update' => 0,
        'grant_delete' => 0,
      ))
      ->execute();
  }

  if (!isset($batch)) {
    drupal_set_message(t('Content permissions have been rebuilt.'));
    node_access_needs_rebuild(FALSE);
    cache_clear_all();
  }
}
